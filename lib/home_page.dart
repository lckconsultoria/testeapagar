import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ValueNotifier<int> position = ValueNotifier(0);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('TESTE'),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ValueListenableBuilder(
              valueListenable: position,
              builder: (context, value, child) {
                print('Position ${position.value}');
                return Text('Position ${position.value}');
              },
            ),
            IconButton(
              onPressed: () {
                for (var i = 0; i < 1000; i++) {
                  position.value++;
                }
              },
              icon: const Icon(Icons.wifi_protected_setup_sharp),
            ),
          ],
        ),
      ),
    );
  }
}
